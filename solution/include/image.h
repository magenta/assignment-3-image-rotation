#ifndef LAB_3_IMAGE_H
#define LAB_3_IMAGE_H
#include <stddef.h>
#include <stdint.h>
#define OOM_ERROR 69
#define RGB_TRIPLE 3

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct __attribute__((packed)) pixel { uint8_t b, g, r; };

void free_image(struct image* img);
uint64_t get_padding(uint64_t width);
struct pixel* malloc_pixels(size_t width, size_t height);
void malloc_check(void *ptr);

#endif //LAB_3_IMAGE_H
