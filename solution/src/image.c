#include "image.h"
#include <stdio.h>
#include <stdlib.h>

void free_image(struct image* img) {
    free(img->data);
}

uint64_t get_padding(uint64_t width) {
    uint64_t padding =  (4 - ((width * RGB_TRIPLE) % 4) % 4);
    return padding;
}

void malloc_check(void *ptr) {
    if (ptr == NULL) {
        fprintf(stderr, "Out of memory error");
        exit(OOM_ERROR);
    }
}

struct pixel* malloc_pixels(size_t width, size_t height) {
    struct pixel* pixels = (struct pixel*) malloc(sizeof(struct pixel) * width * height);
    malloc_check(pixels);
    return pixels;
}


