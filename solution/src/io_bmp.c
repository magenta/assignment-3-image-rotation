#include "bmp.h"
#define BMP_BITS 24
#define BMP_SIGNATURE 0x4D42
#define BMP_RESERVED 0
#define BMP_HEADER_SIZE 40
#define BMP_PLANES 1
#define BMP_PEELS 2834
#define BMP_COMPRESSION 0
#define BMP_COLORS 0

enum read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header header = {0};
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }

    if (header.bfType != BMP_SIGNATURE) {
        return READ_INVALID_SIGNATURE;
    }

    if (header.biBitCount != BMP_BITS) {
        return READ_INVALID_BITS;
    }

    img->width = header.biWidth;
    img->height = header.biHeight;
    img->data = malloc_pixels(img->width, img->height);

    if (img->data == NULL) {
        return READ_MEMORY_ERROR;
    }

    for (uint64_t i = 0; i < img->height; i++) {
        if (fread(img->data + i * img->width, RGB_TRIPLE, img->width, in) != img->width) {
            return READ_INVALID_BITS;
        }

        uint64_t padding = get_padding(img->width);
        uint64_t result = fseek(in, (long) padding, SEEK_CUR);
        if (result != 0) {
            return READ_INVALID_BITS;
        }
    }

    return READ_OK;
}

static struct bmp_header create_header(const struct image *img) {
    return (struct bmp_header) {
            .bfType = BMP_SIGNATURE,
            .bfReserved = BMP_RESERVED,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BMP_HEADER_SIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = BMP_PLANES,
            .biBitCount = BMP_BITS,
            .biCompression = BMP_COMPRESSION,
            .biSizeImage = (img->width)*(img->height) + get_padding(img->width)*(img->height),
            .bfileSize = sizeof(struct bmp_header) + RGB_TRIPLE * (img->width)*(img->height) + get_padding(img->width)*(img->height),
            .biXPelsPerMeter = BMP_PEELS,
            .biYPelsPerMeter = BMP_PEELS,
            .biClrUsed = BMP_COLORS,
            .biClrImportant = BMP_COLORS
    };
}
enum write_status to_bmp( FILE* out, struct image const* img ) {
    struct bmp_header header = create_header(img);

    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1 || ferror(out)) {
        fclose(out);
        return WRITE_ERROR;
    }

    for (uint64_t row = 0; row < img->height; row++) {
        uint64_t written = fwrite(img->data + row * img->width, RGB_TRIPLE, img->width, out);
        if (written != img->width || ferror(out)) {
            fclose(out);
            return WRITE_ERROR;
        }

        uint64_t padding = get_padding(img->width);
        for (uint64_t i = 0; i < padding; i++) {
            if (fputc(0x00, out) == EOF || ferror(out)) {
                fclose(out);
                return WRITE_ERROR;
            }
        }
    }

    return WRITE_OK;
}
