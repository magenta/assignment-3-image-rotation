#include "bmp.h"
#include "image.h"
#include "rotate.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
    if (argc < 3) {
        fprintf(stderr, "Not enough parameters. Enter <source-image>, <transformed-image> and <angle>\n");
        return 1;
    }

    FILE *in = fopen(argv[1], "rb");
    if (in == NULL) {
        fprintf(stderr, "No input file\n");
        return 1;
    }

    struct image image = {0};
    if (from_bmp(in, &image) != READ_OK) {
        fprintf(stderr, "Smth bad :C\n");
        free_image(&image);
        fclose(in);
        return 1;
    }

    fclose(in);
    int angle = (atoi(argv[3]) + 360) % 360;
    int valid_angles[] = {0, 90, 180, 270, -90, -180, -270};
    bool is_valid = false;
    for (int i = 0; i < sizeof(valid_angles); i++) {
        if (angle == valid_angles[i]) {
            is_valid = true;
            break;
        }
    }

    if (!is_valid) {
        free_image(&image);
        fprintf(stderr, "Invalid angle. Enter: 0, 90, -90, 180, -180, 270, -270\n");
        return 1;
    }

    int rotations = (4 - angle / 90) % 4;
    struct image rotated_image;
    for (int i = 0; i < rotations; i++) {
        rotated_image = rotate(image);
        free_image(&image);
        image = rotated_image;
    }

    FILE *out = fopen(argv[2], "wb");
    if (out == NULL) {
        fprintf(stderr, "No output file\n");
        free_image(&image);
        return 1;
    }
    if (to_bmp(out, &image) != WRITE_OK) {
        fprintf(stderr, "Smth bad again :C\n");
        free_image(&image);
        fclose(out);
        return 1;
    }

    free_image(&image);
    fclose(out);
    return 0;
}
