#include "image.h"
#include "rotate.h"

struct image rotate( struct image const source ) {
    struct image rotated_image;
    rotated_image.width = source.height;
    rotated_image.height = source.width;
    rotated_image.data = malloc_pixels(rotated_image.width, rotated_image.height);

    unsigned int r, c;
    for (r = 0; r < source.height; r++) {
        for (c = 0; c < source.width; c++) {
            unsigned int rotated_r = c;
            unsigned int rotated_c = rotated_image.width - r - 1;

            rotated_image.data[rotated_r * rotated_image.width + rotated_c] = source.data[r * source.width + c];
        }
    }

    return rotated_image;
}

